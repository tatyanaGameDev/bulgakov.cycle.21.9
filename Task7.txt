using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task7 : MonoBehaviour
{
    int a = 40;
    int i;
    void Start()
    {
        while (a < 51)
        {
            i = a;

            while (i > 1)
            {
                if (i % 2 == 0)
                {
                    i /= 2;
                }
                else
                {
                    i *= 3;
                    i += 1;
                    i /= 2;
                }
                Debug.Log(i);
            }
        }
    }

}
